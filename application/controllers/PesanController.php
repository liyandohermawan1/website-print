<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PesanController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$model = array('PesanModel','BayarModel');
		$this->load->model($model);
		if (!$this->session->has_userdata('session_id')) {
			$this->session->set_flashdata('alert', 'belum_login');
			redirect(base_url('login'));
		}
	}

	public function pesanUndangan(){
		if (isset($_POST['keranjang'])) {
			$undanganId = 'CRD-' . substr(time(), 5);
			$bahan = $this->input->post('bahan');
			$model = $this->input->post('model');
			$jumlah = $this->input->post('jumlah');
			$estimasi = $this->input->post('estimasi');
			$keterangan = $this->input->post('keterangan');
			$total = 0;
			if ($bahan == 'biasa') {
				$total =  $jumlah * 1000;
			} elseif ($bahan == 'bagus') {
				$total =  $jumlah * 2500;
			}

			$config['upload_path'] = './assets/images/undangan/';
			$config['allowed_types'] = 'jpg|png|jpeg';
			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('upload')) {
				$error = array('error' => $this->upload->display_errors());
				var_dump($error);
			} else {
				$foto = $this->upload->data('file_name');

				$dataUndangan = array(
					'undangan_id' => $undanganId,
					'undangan_bahan' => $bahan,
					'undangan_model' => $model,
					'undangan_jumlah' => $jumlah,
					'undangan_estimasi' => $estimasi,
					'undangan_total' => $total,
					'undangan_foto' => $foto,
					'undangan_keterangan' => $keterangan,
				);

				$allCart = $this->BayarModel->lihat_keranjang();
				$undoneCart = $this->BayarModel->lihat_keranjang_status($this->session->userdata('session_id'),'belum')->row_array();

				if ($allCart == null){
					$cartId = 'CRT-' . substr(time(), 5);
					$dataUndangan['undangan_keranjang_id'] = $cartId;
					$dataCart = array(
						'keranjang_id' => $cartId,
						'keranjang_pengguna_id' => $this->session->userdata('session_id'),
						'keranjang_total' => $total,
					);
					$this->PesanModel->simpan_undangan($dataUndangan);
					$this->BayarModel->simpan_keranjang($dataCart);
					$this->session->set_flashdata('alert', 'pesan_sukses');
					redirect('undangan');
				} else {
					if ($undoneCart != null){
						$cartId = $undoneCart['keranjang_id'];
						$cartTotal = $undoneCart['keranjang_total'];
						$dataUndangan['undangan_keranjang_id'] = $cartId;
						$dataCart['keranjang_total'] = $cartTotal + $total;

						$this->PesanModel->simpan_undangan($dataUndangan);
						$this->BayarModel->update_keranjang($cartId,$dataCart);
						$this->session->set_flashdata('alert', 'pesan_sukses');
						redirect('undangan');
					} else {
						$cartId = 'CRT-' . substr(time(), 5);
						$dataUndangan['undangan_keranjang_id'] = $cartId;
						$dataCart = array(
							'keranjang_id' => $cartId,
							'keranjang_pengguna_id' => $this->session->userdata('session_id'),
							'keranjang_total' => $total,
						);
						$this->PesanModel->simpan_undangan($dataUndangan);
						$this->BayarModel->simpan_keranjang($dataCart);
						$this->session->set_flashdata('alert', 'pesan_sukses');
						redirect('undangan');
					}
				}
			}
		}
		$data = array(
			'title' => 'Cetak undangan'
		);
		$this->load->view('frontend/templates/header',$data);
		$this->load->view('frontend/pesanan/undangan');
		$this->load->view('frontend/templates/footer');
	}

	public function hapusUndangan($id){
		$undangan = $this->PesanModel->lihat_undangan_by_id($id);
		$keranjang_id = $undangan['undangan_keranjang_id'];
		$keranjang = $this->BayarModel->lihat_keranjang_by_id($keranjang_id);
		$total = $undangan['undangan_total'];
		$data = array(
			'keranjang_total' => $keranjang['keranjang_total'] - $total
		);
		$this->PesanModel->delete('undangan_id',$id,'undangan');
		$this->BayarModel->update_keranjang($keranjang_id,$data);
		$this->session->set_flashdata('alert', 'pesan_hapus');
		redirect('keranjang');
	}
	}
