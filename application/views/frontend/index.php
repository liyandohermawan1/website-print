 <!-- banner-section start -->
 <section class="banner-section">
    <div class="banner-elements-image anim-bounce"><img src="assets/frontend/images/elements/banner.png" alt="image"></div>
    <div class="container">
      <div class="row">
        <div class="col-xl-8">
          <div class="banner-content-area">
            <div class="banner-content">
              <span class="banner-sub-title">Selamat Datang</span>
              <h2 class="banner-title" >MENGAPA MEMILIH PERCETAKAN KAMI?</h2>
              <p>Menerima pesanan Undangan pernikahan dalam jumlah besar maupun kecil</p>
            </div>
            <div class="banner-btn-area">
              <a href="#0" class="btn btn-primary btn-round">get started now!</a>
              <a href="https://www.youtube.com/embed/aFYlAzQHnY4" data-rel="lightcase:myCollection" class="video-btn">
                <span class="icon"><i class="icofont-ui-play"></i></span>
                <span class="text">watch video</span>
              </a>
            </div>
        </div>
      </div>
    </div>
    </div>
  </section>
  <!-- banner-section end -->

  <!-- counter-section start -->
  <div class="counter-sections">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="counter-area d-flex justify-content-between">
            <div class="counter-item">
              <div class="counter-icon">
                <img src="assets/frontend/images/icons/counter/1.svg" alt="icon">
              </div>
              <div class="counter-content">
                <span>$</span>
                <span class="counter">961</span>
                <span>k</span>
                <span class="caption">Model Undangan</span>
              </div>
            </div><!-- counter-item end -->
            <div class="counter-item">
              <div class="counter-icon">
                <img src="assets/frontend/images/icons/counter/2.svg" alt="icon">
              </div>
              <div class="counter-content">
                <span>$</span>
                <span class="counter">634</span>
                <span>k</span>
                <span class="caption">Registered Members</span>
              </div>
            </div><!-- counter-item end -->
            <div class="counter-item">
              <div class="counter-icon">
                <img src="assets/frontend/images/icons/counter/3.svg" alt="icon">
              </div>
              <div class="counter-content">
                <span>$</span>
                <span class="counter">14</span>
                <span>k</span>
                <span class="caption">Average Investment</span>
              </div>
            </div><!-- counter-item end -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- counter-section end -->
 <!-- choose-us-section start -->
 <section class="choose-us-section pt-120 pb-120 bg_img" data-background="assets/frontend/images/elements/choose-us-bg-shape.png">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <div class="section-header text-center">
            <span class="section-subtitle">Boost your Money</span>
            <h2 class="section-title">Why Should Choose Us</h2>
            <p>Our service gives you better result and savings, as per your requirement and you can manage youer investments from anywhere either form home or work place,any time.</p>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid p-0">
      <div class="row m-0">
        <div class="col-lg-12 p-0">
          <div class="choose-us-slider owl-carousel">
              <div class="choose-item text-center">
                <div class="choose-thumb">
                  <img src="assets/frontend/images/choose-us/1.png" alt="image">
                </div>
                <div class="choose-content">
                  <h3 class="title">Fast Profit </h3>
                  <p>We're talking about ways you can make money fast.Invest money and get reward, bonus and profit</p>
                  <a href="#0" class="read-more-btn">read more<i class="fa fa-long-arrow-right"></i></a>
                </div>
              </div><!-- choose-item end -->
              <div class="choose-item text-center">
                <div class="choose-thumb">
                  <img src="assets/frontend/images/choose-us/2.png" alt="image">
                </div>
                <div class="choose-content">
                  <h3 class="title">Instant Withdraw</h3>
                  <p>We’re extremely excited to launch instant withdrawals.you can deposit and withdraw funds in just a few clicks.</p>
                  <a href="#0" class="read-more-btn">read more<i class="fa fa-long-arrow-right"></i></a>
                </div>
              </div><!-- choose-item end -->
              <div class="choose-item text-center">
                <div class="choose-thumb">
                  <img src="assets/frontend/images/choose-us/3.png" alt="image">
                </div>
                <div class="choose-content">
                  <h3 class="title">Dedicated Server</h3>
                  <p>Dedicated server hosting with 100% guaranteed network uptime.There's no sharing of CPU time, RAM or bandwidth</p>
                  <a href="#0" class="read-more-btn">read more<i class="fa fa-long-arrow-right"></i></a>
                </div>
              </div><!-- choose-item end -->
              <div class="choose-item text-center">
                <div class="choose-thumb">
                  <img src="assets/frontend/images/choose-us/4.png" alt="image">
                </div>
                <div class="choose-content">
                  <h3 class="title">DDoS Protection</h3>
                  <p>To protect your resources from modern DDoS attacks is through a multi-layer deployment of purpose-built DDoS mitigation </p>
                  <a href="#0" class="read-more-btn">read more<i class="fa fa-long-arrow-right"></i></a>
                </div>
              </div><!-- choose-item end -->
              <div class="choose-item text-center">
                <div class="choose-thumb">
                  <img src="assets/frontend/images/choose-us/5.png" alt="image">
                </div>
                <div class="choose-content">
                  <h3 class="title">24/7 Support</h3>
                  <p>Our Technical Support team is available for any questions.Our  multilingual 24/7 support allows to keep in touch.</p>
                  <a href="#0" class="read-more-btn">read more<i class="fa fa-long-arrow-right"></i></a>
                </div>
              </div><!-- choose-item end -->
              <div class="choose-item text-center">
                <div class="choose-thumb">
                  <img src="assets/frontend/images/choose-us/1.png" alt="image">
                </div>
                <div class="choose-content">
                  <h3 class="title">Fast Profit </h3>
                  <p>We're talking about ways you can make money fast.Invest money and get reward, bonus and profit</p>
                  <a href="#0" class="read-more-btn">read more<i class="fa fa-long-arrow-right"></i></a>
                </div>
              </div><!-- choose-item end -->
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- choose-us-section end -->

  <!-- features-section start -->
  <section class="features-section pt-120 pb-120 section-md-bg">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <div class="section-header text-center">
            <span class="section-subtitle">Our Amazing Features</span>
            <h2 class="section-title">Investing For Everyone</h2>
            <p>We are worldwide investment company who are committed to the principle of revenue maximization and reduction of the financial risks at investing.</p>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-xl-4">
          <div class="feature-thumb anim-bounce">
            <img src="assets/frontend/images/elements/features.png" alt="image">
          </div>
        </div>
        <div class="col-xl-4 offset-md-1 feature-item-wrapper">
          <div class="feature-item wow fadeIn" data-wow-duration="2s" data-wow-delay="0.3s">
            <div class="icon">
              <div class="icon-inner">
                <img src="assets/frontend/images/icons/investment/1.svg" alt="icon">
              </div>
            </div>
            <div class="content">
              <h3 class="title">Sign up in minutes</h3>
              <p>Open an investment account in minutes and get started with as little as $5.</p>
              <a href="#0">get strated</a>
            </div>
          </div><!-- feature-item end -->
          <div class="feature-item wow fadeIn" data-wow-duration="2s" data-wow-delay="0.5s">
            <div class="icon">
              <div class="icon-inner">
                <img src="assets/frontend/images/icons/investment/2.svg" alt="icon">
              </div>
            </div>
            <div class="content">
              <h3 class="title">Investing Made Easy</h3>
              <p>Choose from three simple starting option - cautious , balanced & adventurous.We’ll take care of the rest!</p>
              <a href="#0">read more</a>
            </div>
          </div><!-- feature-item end -->
          <div class="feature-item wow fadeIn" data-wow-duration="2s" data-wow-delay="0.7s">
            <div class="icon">
              <div class="icon-inner">
                <img src="assets/frontend/images/icons/investment/3.svg" alt="icon">
              </div>
            </div>
            <div class="content">
              <h3 class="title">Build Your Porfolio</h3>
              <p>We’ll help you pick an investment strategy that reflects your interests,beliefs and goals.</p>
              <a href="#0">explore our investing </a>
            </div>
          </div><!-- feature-item end -->
        </div>
      </div>
    </div>
  </section>
  <!-- features-section end -->

  <!-- invest-section start -->
  <section class="invest-section pt-120 pb-120 bg_img" data-background="assets/frontend/images/bg-1.png">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-12">
          <div class="section-header text-center text-white">
            <span class="section-subtitle">The smarter way to invest!</span>
            <h2 class="section-title">Start investing! It’s never too late</h2>
            <p>Make sound investment decisions with the help of our research & analytical assets.The minimum  deposit is $5, and maximum is $100,000. We pay 7 days per week.  You may make additional deposits at any time. All our payments are instant payments.</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="invest-table-area wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
            <table>
              <thead>
                <tr>
                  <th>share</th>
                  <th>price</th>
                  <th>growth</th>
                  <th>daily dividend</th>
                  <th>investors</th>
                  <th>time remainig</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <div class="person-details">
                      <div class="thumb"><img src="assets/frontend/images/invest/1.png" alt="image"></div>
                      <div class="content">
                        <span class="name">Forex Stable Income</span>
                      </div>
                    </div>
                  </td>
                  <td>
                    <span class="price">$10.50</span>
                  </td>
                  <td>
                    <span class="growth">122% <i class="fa fa-arrow-up"></i></span>
                  </td>
                  <td>
                    <span class="daily-dividend">$0.9</span>
                  </td>
                  <td>
                    <span class="investors-amount">718</span>
                  </td>
                  <td>
                    <span class="remaining-time">21 DAYS</span>
                    <a href="#0" class="btn btn-primary btn-round pull-right">invest now</a>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div class="person-details">
                      <div class="thumb"><img src="assets/frontend/images/invest/2.png" alt="image"></div>
                      <div class="content">
                        <span class="name">Pavimentadora Real Ltd</span>
                      </div>
                    </div>
                  </td>
                  <td>
                    <span class="price">$10.50</span>
                  </td>
                  <td>
                    <span class="growth">122% <i class="fa fa-arrow-up"></i></span>
                  </td>
                  <td>
                    <span class="daily-dividend">$0.9</span>
                  </td>
                  <td>
                    <span class="investors-amount">718</span>
                  </td>
                  <td>
                    <span class="remaining-time">21 DAYS</span>
                    <a href="#0" class="btn btn-primary btn-round pull-right">invest now</a>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div class="person-details">
                      <div class="thumb"><img src="assets/frontend/images/invest/3.png" alt="image"></div>
                      <div class="content">
                        <span class="name">Ethereum Farm</span>
                      </div>
                    </div>
                  </td>
                  <td>
                    <span class="price">$10.50</span>
                  </td>
                  <td>
                    <span class="growth">122% <i class="fa fa-arrow-up"></i></span>
                  </td>
                  <td>
                    <span class="daily-dividend">$0.9</span>
                  </td>
                  <td>
                    <span class="investors-amount">718</span>
                  </td>
                  <td>
                    <span class="remaining-time">21 DAYS</span>
                    <a href="#0" class="btn btn-primary btn-round pull-right">invest now</a>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div class="person-details">
                      <div class="thumb"><img src="assets/frontend/images/invest/4.png" alt="image"></div>
                      <div class="content">
                        <span class="name">Legacinet Works</span>
                      </div>
                    </div>
                  </td>
                  <td>
                    <span class="price">$10.50</span>
                  </td>
                  <td>
                    <span class="growth">122% <i class="fa fa-arrow-up"></i></span>
                  </td>
                  <td>
                    <span class="daily-dividend">$0.9</span>
                  </td>
                  <td>
                    <span class="investors-amount">718</span>
                  </td>
                  <td>
                    <span class="remaining-time">21 DAYS</span>
                    <a href="#0" class="btn btn-primary btn-round pull-right">invest now</a>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div class="person-details">
                      <div class="thumb"><img src="assets/frontend/images/invest/2.png" alt="image"></div>
                      <div class="content">
                        <span class="name">Healthcare Tech</span>
                      </div>
                    </div>
                  </td>
                  <td>
                    <span class="price">$10.50</span>
                  </td>
                  <td>
                    <span class="growth">122% <i class="fa fa-arrow-up"></i></span>
                  </td>
                  <td>
                    <span class="daily-dividend">$0.9</span>
                  </td>
                  <td>
                    <span class="investors-amount">718</span>
                  </td>
                  <td>
                    <span class="remaining-time">21 DAYS</span>
                    <a href="#0" class="btn btn-primary btn-round pull-right">invest now</a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="btn-area mt-50 text-center">
            <a href="#0" class="btn btn-primary btn-hover text-small">browse more</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- invest-section end -->

  <!-- offer-section start -->
  <section class="offer-section pt-120 pb-120 bg_img" data-background="assets/frontend/images/offer-bg.png">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <div class="section-header text-center wow fadeIn" data-wow-duration="0.8s" data-wow-delay="0.5s">
            <span class="section-subtitle">Our mission is to help our User</span>
            <h2 class="section-title">To Maximize Money</h2>
            <a href="#0" class="btn btn-primary btn-hover mt-30">what we offer</a>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="offer-slider owl-carousel">
            <div class="offer-item">
              <div class="icon">
                <img src="assets/frontend/images/icons/offer/1.svg" alt="icon">
              </div>
              <div class="content">
                <h3 class="title">smart deposit</h3>
                <p>Best way t o put your idle money to work.</p>
                <a href="#0" class="read-more-btn">read more<i class="icofont-long-arrow-right"></i></a>
              </div>
            </div><!-- offer-item end -->
            <div class="offer-item">
              <div class="icon">
                <img src="assets/frontend/images/icons/offer/2.svg" alt="icon">
              </div>
              <div class="content">
                <h3 class="title">One - Tap Invest</h3>
                <p>Invest without net baning/debit card.</p>
                <a href="#0" class="read-more-btn">read more<i class="icofont-long-arrow-right"></i></a>
              </div>
            </div><!-- offer-item end -->
            <div class="offer-item">
              <div class="icon">
                <img src="assets/frontend/images/icons/offer/3.svg" alt="icon">
              </div>
              <div class="content">
                <h3 class="title">invest & saving</h3>
                <p>Grow your saving by investing as little as $5</p>
                <a href="#0" class="read-more-btn">read more<i class="icofont-long-arrow-right"></i></a>
              </div>
            </div><!-- offer-item end -->
            <div class="offer-item">
              <div class="icon">
                <img src="assets/frontend/images/icons/offer/1.svg" alt="icon">
              </div>
              <div class="content">
                <h3 class="title">smart deposit</h3>
                <p>Best way t o put your idle money to work.</p>
                <a href="#0" class="read-more-btn">read more<i class="icofont-long-arrow-right"></i></a>
              </div>
            </div><!-- offer-item end -->
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- offer-section end -->

  <!-- calculate-profit-section start -->
  <section class="calculate-profit-section pt-120 pb-120">
    <div class="bg_img" data-background="assets/frontend/images/invest-plan.jpg"></div>
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <div class="section-header text-center text-white">
            <span class="section-subtitle">Calculate the amazing profits</span>
            <h2 class="section-title">You Can Earn</h2>
            <p>Find out what the returns on your current investments will be valued at, in future. All our issuers have obligation to pay dividends for first year regardless their financial situation that your investments are 100% secured. Calculate your profit from a share using our calculator:</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="calculate-area wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.5s">
            <ul class="nav nav-tabs justify-content-around" id="calculatorTab" role="tablist">
              <li>
                <div class="icon"><img src="assets/frontend/images/icons/invest-calculate/1.svg" alt="icon-image"></div>
                <h5 class="package-name">Basic Plan</h5>
                <span class="percentage">2%</span>
                <a class="active" id="basic-tab" data-toggle="tab" href="#basic" role="tab" aria-controls="basic" aria-selected="true">calculate</a>
              </li>
              <li>
                  <div class="icon"><img src="assets/frontend/images/icons/invest-calculate/2.svg" alt="icon-image"></div>
                  <h5 class="package-name">satandard Plan</h5>
                  <span class="percentage">3%</span>
                <a id="satandard-tab" data-toggle="tab" href="#satandard" role="tab" aria-controls="satandard" aria-selected="false">calculate</a>
              </li>
              <li>
                  <div class="icon"><img src="assets/frontend/images/icons/invest-calculate/3.svg" alt="icon-image"></div>
                  <h5 class="package-name">premium Plan</h5>
                  <span class="percentage">2%</span>
                <a id="premium-tab" data-toggle="tab" href="#premium" role="tab" aria-controls="premium" aria-selected="false">calculate</a>
              </li>
            </ul>
            <div class="tab-content" id="calculatorTabContent">
              <div class="tab-pane fade show active" id="basic" role="tabpanel" aria-labelledby="basic-tab">
                <div class="invest-amount-area text-center">
                  <h4 class="title">Invest Amount</h4>
                  <div class="main-amount">
                    <input type="text" class="calculator-invest" id="basic-amount" readonly>
                  </div>
                  <div id="slider-range-min-one" class="invest-range-slider"></div>
                </div><!-- invest-amount-area end -->
                <div class="plan-amount-calculate">
                  <div class="item">
                    <span class="caption">Basic plan</span>
                    <span class="details">Minimum Deposit $5</span>
                  </div>
                  <div class="item">
                    <span class="profit-amount">$12.67</span>
                    <span class="profit-details">Daily Profit</span>
                  </div>
                  <div class="item">
                    <span class="profit-amount">$12.67</span>
                    <span class="profit-details">per month</span>
                  </div>
                  <div class="item">
                    <a href="#" class="invest-btn btn-round">invest now</a>
                  </div>
                </div><!-- plan-amount-calculate end -->
              </div>
              <div class="tab-pane fade" id="satandard" role="tabpanel" aria-labelledby="satandard-tab">
                <div class="invest-amount-area text-center">
                  <h4 class="title">Invest Amount</h4>
                  <div class="main-amount">
                    <input type="text" class="calculator-invest" id="satandard-amount" readonly>
                  </div>
                  <div id="slider-range-min-two" class="invest-range-slider"></div>
                </div><!-- invest-amount-area end -->
                <div class="plan-amount-calculate">
                  <div class="item">
                    <span class="caption">satandard plan</span>
                    <span class="details">Minimum Deposit $15</span>
                  </div>
                  <div class="item">
                    <span class="profit-amount">$12.67</span>
                    <span class="profit-details">Daily Profit</span>
                  </div>
                  <div class="item">
                    <span class="profit-amount">$12.67</span>
                    <span class="profit-details">per month</span>
                  </div>
                  <div class="item">
                    <a href="#" class="invest-btn btn-round">invest now</a>
                  </div>
                </div><!-- plan-amount-calculate end -->
              </div>
              <div class="tab-pane fade" id="premium" role="tabpanel" aria-labelledby="premium-tab">
                <div class="invest-amount-area text-center">
                  <h4 class="title">Invest Amount</h4>
                  <div class="main-amount">
                    <input type="text" class="calculator-invest" id="premium-amount" readonly>
                  </div>
                  <div id="slider-range-min-three" class="invest-range-slider"></div>
                </div><!-- invest-amount-area end -->
                <div class="plan-amount-calculate">
                  <div class="item">
                    <span class="caption">premium plan</span>
                    <span class="details">Minimum Deposit $25</span>
                  </div>
                  <div class="item">
                    <span class="profit-amount">$12.67</span>
                    <span class="profit-details">Daily Profit</span>
                  </div>
                  <div class="item">
                    <span class="profit-amount">$12.67</span>
                    <span class="profit-details">per month</span>
                  </div>
                  <div class="item">
                    <a href="#" class="invest-btn btn-round">invest now</a>
                  </div>
                </div><!-- plan-amount-calculate end -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- calculate-profit-section end -->

  <!-- deposit-withdraw-section start -->
  <section class="deposit-withdraw-section pt-120 pb-120 section-md-bg">
    <div class="circle-object"><img src="assets/frontend/images/elements/deposit-circle-shape.png" alt="image"></div>
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <div class="section-header text-center">
            <span class="section-subtitle">Convenient money</span>
            <h2 class="section-title">Deposit & Withdrawal</h2>
            <p>Depositing or withdrawing money is simple.We support several payment methods, which depend on what country your payment account is located in.</p>
          </div>
        </div>
      </div>
      <div class="row align-items-center">
        <div class="col-lg-6">
          <div class="dep-wth-option-area wow fadeIn" data-wow-duration="0.8s" data-wow-delay="0.5s">
            <span class="circle one"></span>
            <span class="circle two"></span>
            <span class="circle three"></span>
            <span class="circle four"></span>
            <a href="#0" class="card-item">
              <span class="icon"><img src="assets/frontend/images/icons/payment-option/card.svg" alt="image"></span>
              <span class="caption">Credit Card</span>
            </a><!-- card-item end -->
            <a href="#0" class="card-item">
              <span class="icon"><img src="assets/frontend/images/icons/payment-option/paypal.svg" alt="image"></span>
              <span class="caption">Credit Card</span>
            </a><!-- card-item end -->
            <a href="#0" class="card-item">
              <span class="icon"><img src="assets/frontend/images/icons/payment-option/bitcoin.svg" alt="image"></span>
              <span class="caption">Credit Card</span>
            </a><!-- card-item end -->
            <a href="#0" class="card-item">
              <span class="icon"><img src="assets/frontend/images/icons/payment-option/litecoin.svg" alt="image"></span>
              <span class="caption">Credit Card</span>
            </a><!-- card-item end -->
            <a href="#0" class="card-item">
              <span class="icon"><img src="assets/frontend/images/icons/payment-option/ethereum.svg" alt="image"></span>
              <span class="caption">Credit Card</span>
            </a><!-- card-item end -->
            <a href="#0" class="card-item">
              <span class="icon"><img src="assets/frontend/images/icons/payment-option/ripple.svg" alt="image"></span>
              <span class="caption">Credit Card</span>
            </a><!-- card-item end -->
            <a href="#0" class="text-btn">view all option</a>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="feature-item">
            <div class="icon">
              <div class="icon-inner">
                <img src="assets/frontend/images/icons/payment-option/ft1.svg" alt="icon">
              </div>
            </div>
            <div class="content">
              <h3 class="title">No Limits</h3>
              <p>Unlimited maximum withdrawal amount</p>
            </div>
          </div><!-- feature-item end -->
          <div class="feature-item">
            <div class="icon">
              <div class="icon-inner">
                <img src="assets/frontend/images/icons/payment-option/ft2.svg" alt="icon">
              </div>
            </div>
            <div class="content">
              <h3 class="title">Withdrawal in 24 /7</h3>
              <p>Deposit – instantaneous</p>
            </div>
          </div><!-- feature-item end -->
        </div>
      </div>
    </div>
  </section>
  <!-- deposit-withdraw-section end -->

  <!-- community-section start -->
  <section class="community-section bg_img pt-120" data-background="assets/frontend/images/community-bg.png">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-10">
          <div class="section-header text-center text-white wow fadeIn" data-wow-duration="0.8s" data-wow-delay="0.5s">
            <span class="section-subtitle">We support</span>
            <h2 class="section-title">Cryptocurrency Community</h2>
            <p>Access a world of dynamic investment opportunities, buy into businesses you believe in and share in their  success.You may make additional deposits at any time. All our 
              payments are instant payments.</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="community-wrapper">
            <div class="row">
              <div class="col-lg-7">
                <div class="community-item">
                  <div class="icon">
                    <img src="assets/frontend/images/icons/community/1.svg" alt="image">
                  </div>
                  <div class="content">
                    <h3 class="title">Simplicity</h3>
                    <p>We’re eliminating complex user experiences.</p>
                    <a href="#0" class="read-more-btn">read more<i class="icofont-long-arrow-right"></i></a>
                  </div>
                </div><!-- community-item end -->
                <div class="community-item">
                  <div class="icon">
                    <img src="assets/frontend/images/icons/community/2.svg" alt="image">
                  </div>
                  <div class="content">
                    <h3 class="title">security</h3>
                    <p>Enhanced security features like multi-factor </p>
                    <a href="#0" class="read-more-btn">read more<i class="icofont-long-arrow-right"></i></a>
                  </div>
                </div><!-- community-item end -->
                <div class="community-item">
                  <div class="icon">
                    <img src="assets/frontend/images/icons/community/3.svg" alt="image">
                  </div>
                  <div class="content">
                    <h3 class="title">support</h3>
                    <p>Get all the support you need for your Investment</p>
                    <a href="#0" class="read-more-btn">read more<i class="icofont-long-arrow-right"></i></a>
                  </div>
                </div><!-- community-item end -->
              </div>
              <div class="col-lg-5">
                <div class="user-wrapper">
                  <div class="icon">
                    <img src="assets/frontend/images/icons/community/user-icon.svg" alt="image">
                  </div>
                  <span class="caption">18000+ Users</span>
                  <div class="users-area">
                    <span class="user-img"><img src="assets/frontend/images/users/s1.png" alt="image"></span>
                    <span class="user-img"><img src="assets/frontend/images/users/s2.png" alt="image"></span>
                    <span class="user-img"><img src="assets/frontend/images/users/s3.png" alt="image"></span>
                    <span class="user-img"><img src="assets/frontend/images/users/s4.png" alt="image"></span>
                    <span class="user-img"><img src="assets/frontend/images/users/s5.png" alt="image"></span>
                    <span class="user-img"><img src="assets/frontend/images/users/s6.png" alt="image"></span>
                    <span class="user-img"><img src="assets/frontend/images/users/s7.png" alt="image"></span>
                    <a href="#" class="btn btn-primary btn-round">see all</a>
                  </div>
                </div>
                <div class="btn-area text-center">
                  <a href="#0" class="btn btn-secondary">join us</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> 
    </div>
  </section>
  <!-- community-section end -->

  <!-- latest-transaction-section start -->
  <section class="latest-transaction-section pt-120 pb-120">
    <div class="elemets-bg" data-paroller-factor="-0.2" data-paroller-type="foreground" data-paroller-direction="vertical"><img src="assets/frontend/images/withdraw-bg.png" alt=""></div>
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <div class="section-header text-center">
            <span class="section-subtitle">Latest Transaction</span>
            <h2 class="section-title">Withdrawals</h2>
            <p>Our goal is to simplify investing so that anyone can be an investor.Withthis in mind, we  hand-pick the investments we offer on our platform.</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <ul class="nav nav-tabs justify-content-center tab-nav" id="transactionTab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="daily-tab" data-toggle="tab" href="#daily" role="tab" aria-controls="daily" aria-selected="true">Daily</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="monthly-tab" data-toggle="tab" href="#monthly" role="tab" aria-controls="monthly" aria-selected="false">Monthly</a>
            </li>
          </ul>
          <div class="tab-content" id="transactionTabContent">
            <div class="tab-pane fade show active" id="daily" role="tabpanel" aria-labelledby="daily-tab">
              <div class="withdraw-table-area">
                <table>
                  <thead>
                    <tr>
                      <th>name</th>
                      <th>price</th>
                      <th>Daily Dividend</th>
                      <th>AMOUNTs</th>
                      <th>Deposit by</th>
                      <th>Currency</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td data-head="name">
                        <div class="person-details">
                          <div class="thumb"><img src="assets/frontend/images/withdraw/t-1.png" alt="image"></div>
                          <div class="content">
                            <span class="name">Jim Adams</span>
                          </div>
                        </div>
                      </td>
                      <td data-head="price">
                        <span class="price">$10.50</span>
                      </td>
                      <td data-head="daily dividend">
                        <span class="daily-dividend">$10.50</span>
                      </td>
                      <td data-head="amounts">
                        <span class="amount">$0.9</span>
                      </td>
                      <td data-head="Deposit by">
                        <span class="days">21 days</span>
                      </td>
                      <td data-head="Currency">
                        <img src="assets/frontend/images/icons/withdraw/bitcoin.png" alt="icon">
                      </td>
                    </tr>
                    <tr>
                      <td data-head="name">
                        <div class="person-details">
                          <div class="thumb"><img src="assets/frontend/images/withdraw/t-2.png" alt="image"></div>
                          <div class="content">
                            <span class="name">Willie Barton </span>
                          </div>
                        </div>
                      </td>
                      <td data-head="price">
                        <span class="price">$10.50</span>
                      </td>
                      <td data-head="daily dividend">
                        <span class="daily-dividend">$10.50</span>
                      </td>
                      <td data-head="amounts">
                        <span class="amount">$0.9</span>
                      </td>
                      <td data-head="Deposit by">
                        <span class="days">21 days</span>
                      </td>
                      <td data-head="Currency">
                        <img src="assets/frontend/images/icons/withdraw/fire.png" alt="icon">
                      </td>
                    </tr>
                    <tr>
                      <td data-head="name">
                        <div class="person-details">
                          <div class="thumb"><img src="assets/frontend/images/withdraw/t-3.png" alt="image"></div>
                          <div class="content">
                            <span class="name">Kim Mccoy </span>
                          </div>
                        </div>
                      </td>
                      <td data-head="price">
                        <span class="price">$10.50</span>
                      </td>
                      <td data-head="daily dividend">
                        <span class="daily-dividend">$10.50</span>
                      </td>
                      <td data-head="amounts">
                        <span class="amount">$0.9</span>
                      </td>
                      <td data-head="Deposit by">
                        <span class="days">21 days</span>
                      </td>
                      <td data-head="Currency">
                        <img src="assets/frontend/images/icons/withdraw/bitcoin.png" alt="icon">
                      </td>
                    </tr>
                    <tr>
                      <td data-head="name">
                        <div class="person-details">
                          <div class="thumb"><img src="assets/frontend/images/withdraw/t-4.png" alt="image"></div>
                          <div class="content">
                            <span class="name">Sheryl Tran </span>
                          </div>
                        </div>
                      </td>
                      <td data-head="price">
                        <span class="price">$10.50</span>
                      </td>
                      <td data-head="daily dividend">
                        <span class="daily-dividend">$10.50</span>
                      </td>
                      <td data-head="amounts">
                        <span class="amount">$0.9</span>
                      </td>
                      <td data-head="Deposit by">
                        <span class="days">21 days</span>
                      </td>
                      <td data-head="Currency">
                        <img src="assets/frontend/images/icons/withdraw/fan.png" alt="icon">
                      </td>
                    </tr>
                    <tr>
                      <td data-head="name">
                        <div class="person-details">
                          <div class="thumb"><img src="assets/frontend/images/withdraw/t-5.png" alt="image"></div>
                          <div class="content">
                            <span class="name">Jim Adams</span>
                          </div>
                        </div>
                      </td>
                      <td data-head="price">
                        <span class="price">$10.50</span>
                      </td>
                      <td data-head="daily dividend">
                        <span class="daily-dividend">$10.50</span>
                      </td>
                      <td data-head="amounts">
                        <span class="amount">$0.9</span>
                      </td>
                      <td data-head="Deposit by">
                        <span class="days">21 days</span>
                      </td>
                      <td data-head="Currency">
                        <img src="assets/frontend/images/icons/withdraw/dollar.png" alt="icon">
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            
            <div class="tab-pane fade" id="monthly" role="tabpanel" aria-labelledby="monthly-tab">
              <div class="withdraw-table-area">
                <table>
                  <thead>
                    <tr>
                      <th>name</th>
                      <th>price</th>
                      <th>Daily Dividend</th>
                      <th>AMOUNTs</th>
                      <th>Deposit by</th>
                      <th>Currency</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td data-head="name">
                        <div class="person-details">
                          <div class="thumb"><img src="assets/frontend/images/withdraw/t-1.png" alt="image"></div>
                          <div class="content">
                            <span class="name">Jim Adams</span>
                          </div>
                        </div>
                      </td>
                      <td data-head="price">
                        <span class="price">$10.50</span>
                      </td>
                      <td data-head="daily dividend">
                        <span class="daily-dividend">$10.50</span>
                      </td>
                      <td data-head="amounts">
                        <span class="amount">$0.9</span>
                      </td>
                      <td data-head="Deposit by">
                        <span class="days">21 days</span>
                      </td>
                      <td data-head="Currency">
                        <img src="assets/frontend/images/icons/withdraw/bitcoin.png" alt="icon">
                      </td>
                    </tr>
                    <tr>
                      <td data-head="name">
                        <div class="person-details">
                          <div class="thumb"><img src="assets/frontend/images/withdraw/t-2.png" alt="image"></div>
                          <div class="content">
                            <span class="name">Willie Barton </span>
                          </div>
                        </div>
                      </td>
                      <td data-head="price">
                        <span class="price">$10.50</span>
                      </td>
                      <td data-head="daily dividend">
                        <span class="daily-dividend">$10.50</span>
                      </td>
                      <td data-head="amounts">
                        <span class="amount">$0.9</span>
                      </td>
                      <td data-head="Deposit by">
                        <span class="days">21 days</span>
                      </td>
                      <td data-head="Currency">
                        <img src="assets/frontend/images/icons/withdraw/fire.png" alt="icon">
                      </td>
                    </tr>
                    <tr>
                      <td data-head="name">
                        <div class="person-details">
                          <div class="thumb"><img src="assets/frontend/images/withdraw/t-3.png" alt="image"></div>
                          <div class="content">
                            <span class="name">Kim Mccoy </span>
                          </div>
                        </div>
                      </td>
                      <td data-head="price">
                        <span class="price">$10.50</span>
                      </td>
                      <td data-head="daily dividend">
                        <span class="daily-dividend">$10.50</span>
                      </td>
                      <td data-head="amounts">
                        <span class="amount">$0.9</span>
                      </td>
                      <td data-head="Deposit by">
                        <span class="days">21 days</span>
                      </td>
                      <td data-head="Currency">
                        <img src="assets/frontend/images/icons/withdraw/bitcoin.png" alt="icon">
                      </td>
                    </tr>
                    <tr>
                      <td data-head="name">
                        <div class="person-details">
                          <div class="thumb"><img src="assets/frontend/images/withdraw/t-4.png" alt="image"></div>
                          <div class="content">
                            <span class="name">Sheryl Tran </span>
                          </div>
                        </div>
                      </td>
                      <td data-head="price">
                        <span class="price">$10.50</span>
                      </td>
                      <td data-head="daily dividend">
                        <span class="daily-dividend">$10.50</span>
                      </td>
                      <td data-head="amounts">
                        <span class="amount">$0.9</span>
                      </td>
                      <td data-head="Deposit by">
                        <span class="days">21 days</span>
                      </td>
                      <td data-head="Currency">
                        <img src="assets/frontend/images/icons/withdraw/fan.png" alt="icon">
                      </td>
                    </tr>
                    <tr>
                      <td data-head="name">
                        <div class="person-details">
                          <div class="thumb"><img src="assets/frontend/images/withdraw/t-5.png" alt="image"></div>
                          <div class="content">
                            <span class="name">Jim Adams</span>
                          </div>
                        </div>
                      </td>
                      <td data-head="price">
                        <span class="price">$10.50</span>
                      </td>
                      <td data-head="daily dividend">
                        <span class="daily-dividend">$10.50</span>
                      </td>
                      <td data-head="amounts">
                        <span class="amount">$0.9</span>
                      </td>
                      <td data-head="Deposit by">
                        <span class="days">21 days</span>
                      </td>
                      <td data-head="Currency">
                        <img src="assets/frontend/images/icons/withdraw/dollar.png" alt="icon">
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          
          <div class="btn-area text-center">
            <a href="#0" class="btn btn-primary btn-hover text-small">browse more</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- latest-transaction-section end -->

  <!-- affiliate-features-section start -->
  <section class="affiliate-features-section pt-120 pb-120">
    <div class="shape"><img src="assets/frontend/images/elements/affiliate-shape.png" alt="image"></div>
    <div class="container">
      <div class="row">
        <div class="col-xl-6">
          <div class="affiliate-features-content text-xl-left text-center">
            <div class="section-header">
              <span class="section-subtitle">Earn The Big Money</span>
              <h2 class="section-title">Affiliate Program</h2>
              <p>Our affiliate program can increase your income by receiving percentage from the purchases made by your referrals into. </p>
            </div>
            <p>Invite other users (for example, your friends, co-workers, etc.) to join the project. After registration they will be your referrals; and if they purchase any item on our web site you receive 24% reward.</p>
            <a href="#0" class="btn btn-primary btn-hover text-small">read more</a>
          </div>
        </div>
        <div class="col-xl-6">
          <div class="row mb-none-30">
            <div class="col-xl-6 col-lg-4 col-md-6">
              <div class="affiliate-features-item text-center mb-30">
                <div class="icon"><img src="assets/frontend/images/icons/affiliate-features/1.svg" alt="image"></div>
                <span class="subtitle">Enjoy unlimited</span>
                <h3 class="title">Commissions</h3>
                <p>The more User you refer, the more commissions we’ll pay you. Plain and simple.</p>
              </div>
            </div><!-- affiliate-features-item end -->
            <div class="col-xl-6 col-lg-4 col-md-6">
              <div class="affiliate-features-item text-center mb-30">
                <div class="icon"><img src="assets/frontend/images/icons/affiliate-features/2.svg" alt="image"></div>
                <span class="subtitle">Extra Bonus and</span>
                <h3 class="title">Promotions</h3>
                <p>Boost your monthly earnings with additional promotional opportunities.</p>
              </div>
            </div><!-- affiliate-features-item end -->
            <div class="col-xl-6 col-lg-4 col-md-6">
              <div class="affiliate-features-item text-center mb-30">
                <div class="icon"><img src="assets/frontend/images/icons/affiliate-features/3.svg" alt="image"></div>
                <span class="subtitle">Get top notch</span>
                <h3 class="title">Support</h3>
                <p>Maximize your earning potential with our popular amazing support center.</p>
              </div>
            </div><!-- affiliate-features-item end -->
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- affiliate-features-section end -->

  <!-- testimonial-section start -->
  <section class="testimonial-section pt-xl-120 pb-120">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-10">
          <div class="section-header text-center">
            <span class="section-subtitle">Testimonials</span>
            <h2 class="section-title">Over 7,000 Happy Customers</h2>
            <p>We have many happy investors invest with us .Some impresions from our Customers!  PLease read some of the lovely things our Customers say about us.</p>
            <div class="btn-area">
              <a href="#0" class="btn btn-primary text-small">join us</a>
              <a href="#0" class="btn btn-primary text-small">what we offer</a>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="testimonial-wrapper style--one d-lg-flex flex-wrap justify-content-between d-none">
            <div class="testimonial-single">
              <div class="thumb">
                <img src="assets/frontend/images/testimonial/1.png" alt="image">
              </div>
              <div class="details text-center">
                <p>Great service! I have been worried about investing. But when I came here. I don't have to worry anymore</p>
                <h4 class="name">Joy Kelley</h4>
                <span class="client-details">United kingdom, 28th April,2019</span>
                <span class="arrow-wrap"><span class="arrow"></span></span>
              </div>
            </div><!-- testimonial-single end -->
            <div class="testimonial-single">
              <div class="thumb">
                <img src="assets/frontend/images/testimonial/2.png" alt="image">
              </div>
              <div class="details text-center">
                <p>Great service! I have been worried about investing. But when I came here. I don't have to worry anymore</p>
                <h4 class="name">Joy Kelley</h4>
                <span class="client-details">United kingdom, 28th April,2019</span>
                <span class="arrow-wrap"><span class="arrow"></span></span>
              </div>
            </div><!-- testimonial-single end -->
            <div class="testimonial-single">
              <div class="thumb">
                <img src="assets/frontend/images/testimonial/3.png" alt="image">
              </div>
              <div class="details text-center">
                <p>Great service! I have been worried about investing. But when I came here. I don't have to worry anymore</p>
                <h4 class="name">Joy Kelley</h4>
                <span class="client-details">United kingdom, 28th April,2019</span>
                <span class="arrow-wrap"><span class="arrow"></span></span>
              </div>
            </div><!-- testimonial-single end -->
            <div class="testimonial-single active">
              <div class="thumb">
                <img src="assets/frontend/images/testimonial/4.png" alt="image">
              </div>
              <div class="details text-center">
                <p>Great service! I have been worried about investing. But when I came here. I don't have to worry anymore</p>
                <h4 class="name">Joy Kelley</h4>
                <span class="client-details">United kingdom, 28th April,2019</span>
                <span class="arrow-wrap"><span class="arrow"></span></span>
              </div>
            </div><!-- testimonial-single end -->
            <div class="testimonial-single">
              <div class="thumb">
                <img src="assets/frontend/images/testimonial/5.png" alt="image">
              </div>
              <div class="details text-center">
                <p>Great service! I have been worried about investing. But when I came here. I don't have to worry anymore</p>
                <h4 class="name">Joy Kelley</h4>
                <span class="client-details">United kingdom, 28th April,2019</span>
                <span class="arrow-wrap"><span class="arrow"></span></span>
              </div>
            </div><!-- testimonial-single end -->
            <div class="testimonial-single">
              <div class="thumb">
                <img src="assets/frontend/images/testimonial/6.png" alt="image">
              </div>
              <div class="details text-center">
                <p>Great service! I have been worried about investing. But when I came here. I don't have to worry anymore</p>
                <h4 class="name">Joy Kelley</h4>
                <span class="client-details">United kingdom, 28th April,2019</span>
                <span class="arrow-wrap"><span class="arrow"></span></span>
              </div>
            </div><!-- testimonial-single end -->
          </div>
          <div class="testmonial-slider-wrapper d-lg-none mb-4">
            <div class="testimonial-slider owl-carousel">
              <div class="testimonial-single-two">
                <div class="thumb"><img src="assets/frontend/images/testimonial/2.png" alt="image"></div>
                <h4 class="name">Kevin Ohashi</h4>
                <span class="designation">Award winning blogger</span>
                <p>behoof has one of the friendliest affiliate programs.They’re definitely one of the bestinvestment website in the world. I’ve been using them for a long time and have never had any problems</p>
              </div><!-- testimonial-single-two end -->
              <div class="testimonial-single-two">
                <div class="thumb"><img src="assets/frontend/images/testimonial/3.png" alt="image"></div>
                <h4 class="name">Kevin Ohashi</h4>
                <span class="designation">Award winning blogger</span>
                <p>behoof has one of the friendliest affiliate programs.They’re definitely one of the bestinvestment website in the world. I’ve been using them for a long time and have never had any problems</p>
              </div><!-- testimonial-single-two end -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- testimonial-section end -->

  <!-- investors-section start -->
  <section class="investors-section pt-120 pb-120">
    <div class="bg-shape bg_img" data-background="assets/frontend/images/investor-bg.png"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-6 col-md-8">
          <div class="section-header text-white wow fadeIn" data-wow-duration="0.8s" data-wow-delay="0.5s">
            <span class="section-subtitle">Take a look at our</span>
            <h2 class="section-title">Top 10 Investors</h2>
            <p>A look at the top ten investors of all time and the strategies they used to make their money.</p>
          </div>
        </div>
      </div>
      <div class="investor-slider owl-carousel">
        <div class="investor-item text-center">
          <div class="thumb">
            <img src="assets/frontend/images/investors/1.png" alt="image">
            <a href="#0" class="icon"><i class="fa fa-linkedin"></i></a>
          </div>
          <div class="content">
            <h4 class="name"><a href="#0">Sean Obrien</a></h4>
            <span class="amount">$50,000.00</span>
            <p>Pain by <img src="assets/frontend/images/icons/withdraw/bitcoin.png" alt="icon"></p>
          </div>
        </div><!-- investor-item end -->
        <div class="investor-item text-center">
          <div class="thumb">
            <img src="assets/frontend/images/investors/2.png" alt="image">
            <a href="#0" class="icon"><i class="fa fa-linkedin"></i></a>
          </div>
          <div class="content">
            <h4 class="name"><a href="#0">Naomi White</a></h4>
            <span class="amount">$43,500.00</span>
            <p>Pain by <img src="assets/frontend/images/icons/withdraw/bitcoin.png" alt="icon"></p>
          </div>
        </div><!-- investor-item end -->
        <div class="investor-item text-center">
          <div class="thumb">
            <img src="assets/frontend/images/investors/3.png" alt="image">
            <a href="#0" class="icon"><i class="fa fa-linkedin"></i></a>
          </div>
          <div class="content">
            <h4 class="name"><a href="#0">Tom Barker</a></h4>
            <span class="amount">$42,000.00</span>
            <p>Pain by <img src="assets/frontend/images/icons/withdraw/bitcoin.png" alt="icon"></p>
          </div>
        </div><!-- investor-item end -->
        <div class="investor-item text-center">
          <div class="thumb">
            <img src="assets/frontend/images/investors/1.png" alt="image">
            <a href="#0" class="icon"><i class="fa fa-linkedin"></i></a>
          </div>
          <div class="content">
            <h4 class="name"><a href="#0">Sean Obrien</a></h4>
            <span class="amount">$50,000.00</span>
            <p>Pain by <img src="assets/frontend/images/icons/withdraw/bitcoin.png" alt="icon"></p>
          </div>
        </div><!-- investor-item end -->
      </div>
    </div>
  </section>
  <!-- investors-section end -->

  <!-- contest-winner-section start  -->
  <section class="contest-winner-section pt-120 pb-120">
    <div class="shape"><img src="assets/frontend/images/elements/contest-shape.png" alt="image"></div>
    <div class="container">
      <div class="row justify-content-between">
        <div class="col-xl-6 col-lg-6">
          <div class="contest-winner-content">
            <div class="section-header text-lg-left text-center">
              <span class="section-subtitle">Take a look at our latest</span>
              <h2 class="section-title">Contest Winners</h2>
              <p>The contest is based on sales from your referrals.The person with the most total  referral's revenue will get the Grand Prize. The more revenue your referrals produce the bigger chance for you to be on top.</p>
              <a href="#0" class="btn btn-primary">read more</a>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-6">
          <div class="contest-winner-slider owl-carousel">
            <div class="contest-winner-item">
              <div class="thumb">
                <img src="assets/frontend/images/contest-winner/1.png" alt="image">
                <span class="icon"><img src="assets/frontend/images/icons/contest-winner/trophy.svg" alt="icon"></span>
                <span class="amount">$1,000.00</span>
                <span class="date">feb 2019</span>
              </div>
            </div><!-- contest-winner-item end -->
            <div class="contest-winner-item">
              <div class="thumb">
                <img src="assets/frontend/images/contest-winner/1.png" alt="image">
                <span class="icon"><img src="assets/frontend/images/icons/contest-winner/trophy.svg" alt="icon"></span>
                <span class="amount">$1,000.00</span>
                <span class="date">feb 2019</span>
              </div>
            </div><!-- contest-winner-item end -->
            <div class="contest-winner-item">
              <div class="thumb">
                <img src="assets/frontend/images/contest-winner/1.png" alt="image">
                <span class="icon"><img src="assets/frontend/images/icons/contest-winner/trophy.svg" alt="icon"></span>
                <span class="amount">$1,000.00</span>
                <span class="date">feb 2019</span>
              </div>
            </div><!-- contest-winner-item end -->
            <div class="contest-winner-item">
              <div class="thumb">
                <img src="assets/frontend/images/contest-winner/1.png" alt="image">
                <span class="icon"><img src="assets/frontend/images/icons/contest-winner/trophy.svg" alt="icon"></span>
                <span class="amount">$1,000.00</span>
                <span class="date">feb 2019</span>
              </div>
            </div><!-- contest-winner-item end -->
            <div class="contest-winner-item">
              <div class="thumb">
                <img src="assets/frontend/images/contest-winner/1.png" alt="image">
                <span class="icon"><img src="assets/frontend/images/icons/contest-winner/trophy.svg" alt="icon"></span>
                <span class="amount">$1,000.00</span>
                <span class="date">feb 2019</span>
              </div>
            </div><!-- contest-winner-item end -->
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- contest-winner-section end  -->

  <!-- partner-section start -->
  <section class="partner-section pt-lg-120 pb-120">
    <div class="container">
      <div class="row justify-content-between">
        <div class="col-lg-5">
          <div class="partner-wrapper">
            <div class="partner-single wow zoomIn" data-wow-duration="0.3s" data-wow-delay="0.5s">
              <div class="partner-single-inner">
                <img src="assets/frontend/images/partners/1.png" alt="image">
              </div>
            </div><!-- partner-single end -->
            <div class="partner-single wow zoomIn" data-wow-duration="0.5s" data-wow-delay="0.5s">
              <div class="partner-single-inner">
                <img src="assets/frontend/images/partners/2.png" alt="image">
              </div>
            </div><!-- partner-single end -->
            <div class="partner-single wow zoomIn" data-wow-duration="0.7s" data-wow-delay="0.5s">
              <div class="partner-single-inner">
                <img src="assets/frontend/images/partners/3.png" alt="image">
              </div>
            </div><!-- partner-single end -->
            <div class="partner-single wow zoomIn" data-wow-duration="0.9s" data-wow-delay="0.5s">
              <div class="partner-single-inner">
                <img src="assets/frontend/images/partners/4.png" alt="image">
              </div>
            </div><!-- partner-single end -->
            <div class="partner-single wow zoomIn" data-wow-duration="1.2s" data-wow-delay="0.5s">
              <div class="partner-single-inner">
                <img src="assets/frontend/images/partners/5.png" alt="image">
              </div>
            </div><!-- partner-single end -->
          </div>
        </div>
        <div class="col-lg-6">
          <div class="section-header text-lg-left text-center">
            <span class="section-subtitle">Let’s see our</span>
            <h2 class="section-title">Trusted Partners</h2>
            <p>We’re committed to making our clients successful by becoming their partners and trusted advisors .behoof believes in being your trusted partner and earning that trust through confidence and performance in service and support.</p>
            <a href="#0" class="btn btn-primary text-small">join with us</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- partner-section end -->
