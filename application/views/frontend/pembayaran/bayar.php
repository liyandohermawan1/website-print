
 <!-- inner-page-banner-section start -->
 <section class="inner-page-banner-section gradient-bg">
        <div class="illustration-img"><img src="<?= base_url() ?>/assets/frontend/images/inner-page-banner-illustrations/about.png" alt="image-illustration"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="inner-page-content-area">
                        <h2 class="page-title">about us</h2>
                        <nav aria-label="breadcrumb" class="page-header-breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="home-one.html">Home</a></li>
                                <li class="breadcrumb-item">result</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- inner-page-banner-section end -->
	<section class="about-section start pt-120 pb-120">
<div class="container">
	<header class="page-header">
		<h1 class="page-title">Checkout Pesanan</h1>
	</header>
	<div class="row row-col-gap" data-gutter="60">
		<div class="col-md-4">
			<h3 class="widget-title">Info Pesanan</h3>
			<div class="box">
				<table class="table">
					<thead>
					<tr>
						<th>Jenis</th>
						<th>Jumlah</th>
						<th>Harga</th>
					</tr>
					</thead>
					<tbody>
				
					<tr>
						<?php
						if ($undangan == !null):
							?>
							<td>Kartu Nama</td>
							<td><?=count($undangan)?></td>
							<td style="text-align: right">
								<?php
								$harga = 0;
								foreach ($undangan as $key=>$value) {
									$harga = $harga + $value['undangan_total'];
								}
								echo 'Rp. '.nominal($harga)
								?>
							</td>
						<?php
						endif;
						?>
					</tr>
					</tbody>
					<tfoot>
					<tr>
						<td colspan="2"><b>Total</b></td>
						<td style="text-align: right"><b>Rp. <?= nominal($pesanan['keranjang_total']) ?></b></td>
					</tr>
					</tfoot>
				</table>
			</div>
		</div>
		<div class="col-md-4">
			<h3 class="widget-title">Pembayaran</h3>
			<div class="box">
				<?=form_open('bayar/'.$pesanan['keranjang_id'])?>
				<p>Pilih Jenis Pembayaran</p>
				<input type="radio" name="tipebayar" value="bri" required> Transfer Bank BRI <br>
				<input type="radio" name="tipebayar" value="bni" required> Transfer Bank BNI <br>
				<br>
				<button type="submit" class="btn btn-primary" name="selesai">Selesai</button>
				<div class="gap gap-small"></div>
				<?=form_close()?>
			</div>
		</div>
	</div>
</div>
</section>