<section class="inner-page-banner-section gradient-bg">
        <div class="illustration-img"><img src="<?= base_url() ?>/assets/frontend/images/inner-page-banner-illustrations/about.png" alt="image-illustration"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="inner-page-content-area">
                        <h2 class="page-title">about us</h2>
                        <nav aria-label="breadcrumb" class="page-header-breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="home-one.html">Home</a></li>
                                <li class="breadcrumb-item">result</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- inner-page-banner-section end -->
	<section class="about-section start pt-120 pb-120">
<?php
if ($keranjang['keranjang_total'] == '0'):
	?>
	<div class="container">
		<div class="text-center"><i class="fa fa-cart-arrow-down empty-cart-icon"></i>
			<p class="lead">Keranjang Kamu Kosong</p><a class="btn btn-primary btn-lg" href="<?= base_url() ?>">Pesan
				Sekarang <i class="fa fa-long-arrow-right"></i></a>
		</div>
	</div>
<?php
elseif ($keranjang == !null):
	?>
	<div class="container">
		<header class="page-header">
			<h1 class="page-title">Keranjang</h1>
		</header>
		<div class="row">
			<div class="col-md-10">
				
				<?php
				if ($undangan != null):
					?>
					<h4>Undangan</h4>
					<table class="table table-bordered table-shopping-cart">
						<thead>
						<tr>
							<th>Foto</th>
							<th>Bahan</th>
							<th>Jumlah</th>
							<th>Estimasi</th>
							<th>Total</th>
							<th>Hapus</th>
						</tr>
						</thead>
						<tbody>
						<?php
						foreach ($undangan as $key => $value):
							?>
							<tr>
								<td><img src="<?= base_url('assets/images/undangan/') . $value['undangan_foto'] ?>" alt="foto"
										 width="300" height="100"></td>
								<td><?= $value['undangan_bahan'] ?></td>
								<td><?= $value['undangan_jumlah'] ?> </td>
								<td><?= $value['undangan_estimasi'] ?> Hari</td>
								<td style="text-align: right"> Rp.<?= nominal($value['undangan_total']) ?></td>
								<td><a class="fa fa-close table-shopping-remove"
									   href="<?= base_url('hapus/undangan/' . $value['undangan_id']) ?>"
									   onclick="return confirm('Hapus Pesanan? ')"></a></td>
							</tr>
						<?php
						endforeach;
						?>
						</tbody>
					</table>
					<div class="gap gap-small"></div>
				<?php
				endif;
				?>
			</div>
			<div class="col-md-2">
				<h4>Total</h4>
				<h3>Rp. <?= nominal($keranjang['keranjang_total']) ?></h3>
				<a class="btn btn-primary" href="<?= base_url('bayar/' . $keranjang['keranjang_id']) ?>">Bayar</a>
			</div>
		</div>
	</div>
<?php
else:
	?>
	<div class="container">
		<div class="text-center"><i class="fa fa-cart-arrow-down empty-cart-icon"></i>
			<p class="lead">Keranjang Kamu Kosong</p><a class="btn btn-primary btn-lg" href="<?= base_url() ?>">Pesan
				Sekarang <i class="fa fa-long-arrow-right"></i></a>
		</div>
	</div>
<?php
endif;
?>
</section>