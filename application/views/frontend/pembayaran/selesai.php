 <!-- inner-page-banner-section start -->
 <section class="inner-page-banner-section gradient-bg">
        <div class="illustration-img"><img src="<?= base_url() ?>/assets/frontend/images/inner-page-banner-illustrations/about.png" alt="image-illustration"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="inner-page-content-area">
                        <h2 class="page-title">about us</h2>
                        <nav aria-label="breadcrumb" class="page-header-breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="home-one.html">Home</a></li>
                                <li class="breadcrumb-item">result</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- inner-page-banner-section end -->
	<section class="about-section start pt-120 pb-120">
<div class="gap"></div>
<div class="container">
	<div class="payment-success-icon fa fa-check-circle-o"></div>
	<div class="payment-success-title-area">
		<h1 class="payment-success-title"><?= $this->session->userdata('session_username');?>, Terima Kasih Telah Memesan</h1>
		<p class="lead">Silahkan transfer ke rekening <b><?=$bank?></b> sebesar <b>Rp. <?=nominal($pesanan['keranjang_total'])?></b> sebelum 1x24 jam.
		</p>
	</div>
	<div class="gap gap-small"></div>
	<div class="gap gap-small"></div>
	<div class="gap gap-small"></div>
	<div class="gap gap-small"></div>
</div>
</section>