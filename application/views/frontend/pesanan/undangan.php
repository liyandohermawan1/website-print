 <!-- inner-page-banner-section start -->
 <section class="inner-page-banner-section gradient-bg">
        <div class="illustration-img"><img src="<?= base_url() ?>/assets/frontend/images/inner-page-banner-illustrations/about.png" alt="image-illustration"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="inner-page-content-area">
                        <h2 class="page-title">about us</h2>
                        <nav aria-label="breadcrumb" class="page-header-breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="home-one.html">Home</a></li>
                                <li class="breadcrumb-item">result</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- inner-page-banner-section end -->
	<section class="about-section start pt-120 pb-120">
<div class="container">
<?= form_open('undangan' , array('enctype' => 'multipart/form-data')) ?>
  <div class="row">
    <div class="col-6">
	<h4>Upload Gambar</h4>
			<div class="product-page-product-wrap">
				<div class="clearfix">
					<input type="file" class="dropify" name="upload" required>
				</div>
			</div>
			<hr>
			<div class="row justify-content-md-center" data-gutter="10">
			<div class="col-md-3">
			<a href="<?=base_url('assets/images/model/01A.PNG')?>" target="_blank"> 
			<img src="<?=base_url('assets/images/model/01A.PNG')?>">
			<label class="center">01A</label></a>
			</div>
			<div class="col-md-3">
			<a href="<?=base_url('assets/images/model/02A.PNG')?>" target="_blank"> 
			<img src="<?=base_url('assets/images/model/02A.PNG')?>">
			<label class="center">02A</label></a>
			</div>
			<div class="col-md-3">
			<a href="<?=base_url('assets/images/model/03A.PNG')?>" target="_blank"> 
			<img src="<?=base_url('assets/images/model/03A.PNG')?>">
			<label class="center">03A</label></a>
			</div>
			<div class="col-md-3">
			<a href="<?=base_url('assets/images/model/04A.PNG')?>" target="_blank"> 
			<img src="<?=base_url('assets/images/model/04A.PNG')?>">
			<label class="center">04A</label></a>
			</div>
			<div class="col-md-3">
			<a href="<?=base_url('assets/images/model/05A.PNG')?>" target="_blank"> 
			<img src="<?=base_url('assets/images/model/05A.PNG')?>">
			<label class="center">05A</label></a>
			</div>
			
			</div>
			
		 </div>
    <div class="col-6">
	<h4>Detail Pesanan</h4>
			<div class="row" data-gutter="10">
				<div class="col-md-8">
					<div class="box">
						<div class="form-group">
							<label for="">Tipe Bahan<span style="color: red">*</span> :</label><br>
							<select name="bahan" id="bahan" class="form-control" required>
								<option value="biasa">Biasa (bct)</option>
								<option value="bagus">Bagus (glossy)</option>
							</select>
						</div>
						<div class="form-group">
							<label for="">Model<span style="color: red">*</span> :</label><br>
							<select name="model"  class="form-control" required>
								<option value="01A">01A</option>
								<option value="02A">02A</option>
								<option value="03A">03A</option>
								<option value="04A">04A</option>
								<option value="05A">05A</option>
							</select>
						</div>
						<div class="form-group">
							<label for="">Jumlah undangan :</label>
							<input type="number" name="jumlah" class="form-control" id="jumlah" onkeyup="showTotalKartu()"
								   required autocomplete="off">
						</div>
						<div class="form-group">
							<label for="">Estimasi Waktu (hari) :</label>
							<input type="number" class="form-control" required autocomplete="off" name="estimasi">
						</div>
							<div class="form-group">
							<label for="">Keterangan</label>
							<textarea type="text" class="form-control" required autocomplete="off" name="keterangan"></textarea>
						</div>
						<br>
						<div class="form-group">
							<label for=""><span style="color: red">*</span>Keterangan :</label>
							<ul>
								<li>Biasa (bct) : Rp. 1000 per satuan</li>
								<li>Bagus (glossy) : Rp. 2500 per satuan</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="box-highlight">
						<h4>Total</h4>
						<div id="total"><h3>0</h3></div>
						<button type="submit" class="btn btn-block btn-primary" name="keranjang"><i
								class="fa fa-shopping-cart"></i>Add to cart
						</button>
					
					</div>
				</div>
			</div>
		 </div>
  </div>
  <?= form_close() ?>
</div>

</section>
