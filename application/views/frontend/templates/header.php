<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from pixner.net/behoof/demo/home-one.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 24 Mar 2020 14:57:32 GMT -->
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title><?=$title?></title>
  <link rel="shortcut icon" type="image/png" href="<?= base_url() ?>assets/frontend/images/favicon.png"/>
  <link rel="stylesheet" href="<?= base_url() ?>assets/frontend/css/fontawesome.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/frontend/css/icofont.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/frontend/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/frontend/css/lightcase.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/frontend/css/owl.carousel.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/frontend/css/jquery-ui.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/frontend/css/nice-select.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/frontend/css/animate.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/frontend/css/style.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/frontend/css/responsive.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/backend/node_modules/dropify/dist/css/dropify.min.css">

</head>
<body>

  <!-- preloader start -->
  <div class="preloader">
    <div class="preloader-box">
      <div>T</div>
      <div>U</div>
      <div>N</div>
      <div>G</div>
      <div>G</div>
      <div>U</div>
      <div>I</div>
      <div>N</div>
    </div>
  </div> 
  <!-- preloader end -->

    <!-- signin-area start -->
    <div class="modal fade" id="signInModal" tabindex="-1" role="dialog"  aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content bdr-radius">
          <div class="signin-wrapper">
            <div class="signin-wrapper-header text-center">
              <div class="logo"><h1><i class="fa fa-print"></i></h1></div>
              <h3 class="title">Login with</h3>
              <p>Welcome back, please sign in below</p>
            </div>
            <?= form_open('login') ?>
              <div class="form-group">
                <label for="signinEmail">Username</label>*</label>
                <input type="text" class="form-control" id="signinEmail" name="username" placeholder="Masukan Username anda">
              </div>
              <div class="form-group">
                <label for="signinPass">Password*</label>
                <input type="password" class="form-control" id="signinPass" name="password" placeholder="Password">
              </div>
			   <input type="submit" class="btn btn-primary btn-hover" value="Login" name="login"/>
			   <?= form_close() ?>
          </div>
        </div>
      </div>
    </div>
    <!-- signin-area end -->
    
    <!-- signup-area start -->
    <div class="modal fade" id="signUpModal" tabindex="-1" role="dialog"  aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content bdr-radius">
          <div class="signin-wrapper">
            <div class="signin-wrapper-header text-center">
              <div class="logo"><img src="assets/frontend/images/elements/logo-icon.png" alt="image"></div>
              <h3 class="title">Login with</h3>
              <p>Welcome back, please sign in below</p>
            </div>
            <form class="signin-form">
              <div class="form-group">
                <label for="signinEmail">Email*</label>
                <input type="email" class="form-control" id="signupEmail" placeholder="Enter your Email">
              </div>
              <div class="form-group">
                <label for="signinPass">Password*</label>
                <input type="password" class="form-control" id="signupPass" placeholder="Password">
              </div>
              <div class="form-group">
                <label for="signinPass">Confirm Password*</label>
                <input type="password" class="form-control" id="signupRePass" placeholder="Password">
              </div>
              <div class="form-group">
                <div class="custom-checkbox">
                  <input type="checkbox" name="id-2" id="id-2" checked>
                  <label for="id-2">I do not want to be kept up to date about relevant investment opportunities, features, and events</label>
                  <span class="checkbox"></span>
                </div>
              </div>
              <button type="submit" class="btn btn-primary btn-hover">Log In</button>
            </form>
            <div class="signin-wrapper-footer">
              <p class="bottom-text">Already have an account?<a href="#0" data-toggle="modal" data-target="#signInModal" data-dismiss="modal" aria-label="Close">Login</a></p>
              <div class="divider"><span>or</span></div>
              <ul class="social-list">
                <li><a href="#0"><i class="fa fa-facebook-f"></i></a></li>
                <li><a href="#0"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#0"><i class="fa fa-linkedin"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- signup-area end -->

  <!--  header-section start  -->
  <header class="header-section">
    <div class="header-top">
      <div class="container">
        <div class="row justify-content-between">
          <div class="col-lg-6 col-md-6 col-sm-6">
            
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6">
                  <div class="header-cart-count">
						<?php
						$ci =& get_instance();
						$ci->load->model('BayarModel');
						$keranjang = $ci->BayarModel->lihat_keranjang_status($this->session->userdata('session_id'), 'belum')->row_array();
						
						if ($keranjang == null):
							?>
							<a href="<?= base_url('keranjang') ?>"><i
									class="fa fa-shopping-cart"></i><span>Keranjang</span></a>
						<?php
						else:
							?>
							<a href="<?= base_url('keranjang') ?>"><i
									class="fa fa-shopping-cart animated infinite tada"></i><span>Keranjang</span></a>
						<?php
						endif;
						?>
				
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
    <div class="header-bottom">
      <div class="container">
        <nav class="navbar navbar-expand-xl">
		<a class="navbar-brand" href="<?= base_url('') ?>">
					<h3><i class="fa fa-print"></i>Cetak Undangan</h3></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="menu-toggle"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav main-menu ml-auto">
              <li class="active"><a href="#">Home</a>
              </li>
              <li class="menu_has_children"><a href="#0">Pesan Sekarang</a>
                <ul class="sub-menu">
							<li><a href="<?= base_url('undangan') ?>"><i class="fa fa-file-image-o dropdown-menu-category-icon"></i>Undangan</a>
							</li>
                </ul>
              </li>
			  <?php if ($this->session->userdata('session_level') == null): ?>
			  <li><a href="<?=base_url('admin')?>" >Login Admin</a></li>
			  <?php else: ?>
						<li class="menu_has_children">
							<a href="<?= base_url('profil') ?>"><i
									class="fa fa-user-circle"></i> <?= $this->session->userdata('session_username') ?>
							</a>
							<ul class="sub-menu">
								<li><a href="<?= base_url('profil') ?>"><i class="fa fa-user" style="width: 20%"></i>
										Profil Saya</a></li>
								<li><a href="<?= base_url('pesanan') ?>"><i class="fa fa-list" style="width: 20%"></i>
										Data Pesanan</a></li>
								<li><a href="<?= base_url('logout') ?>" onclick="return confirm('Logout? ')"><i class="fa fa-sign-out"
																		   style="width: 20%"></i> Logout</a></li>
							</ul>
						</li>
			  <?php endif; ?>
			  
          </ul>
		  <?php if ($this->session->userdata('session_level') == null): ?>
            <div class="header-join-part">
              <button type="button" class="btn btn-danger " data-toggle="modal" data-target="#signInModal">Login</button>
            </div>
			<?php endif; ?>
          </ul>
          </div>
        </nav>
		
      </div>
    </div><!-- header-bottom end -->
  </header>
  <div class="alert-parent">
		<?php if ($this->session->flashdata('alert') == 'login_sukses') : ?>
		<div class="alert-success animated fadeInDownBig hide-it">
			<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
			<i class="fa fa-check"></i> Berhasil Login
		</div>
		<?php elseif ($this->session->flashdata('alert') == 'success_register') : ?>
		<div class="alert-success animated fadeInDownBig hide-it">
			<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
			<i class="fa fa-check"></i> Berhasil Register, Silahkan Login
		</div>
		<?php elseif ($this->session->flashdata('alert') == 'login_gagal') : ?>
		<div class="alert-danger animated fadeInDownBig hide-it">
			<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
			<i class="fa fa-warning"></i> Username atau password salah
		</div>
		<?php elseif ($this->session->flashdata('alert') == 'belum_login') : ?>
		<div class="alert-danger animated fadeInDownBig hide-it">
			<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
			<i class="fa fa-warning"></i> Silahkan Login Dahulu
		</div>
		<?php elseif ($this->session->flashdata('alert') == 'logout_sukses') : ?>
		<div class="alert-success animated fadeInDownBig hide-it">
			<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
			<i class="fa fa-check"></i> Berhasil Logout
		</div>
		<?php elseif ($this->session->flashdata('alert') == 'bayar_sukses') : ?>
		<div class="alert-success animated fadeInDownBig hide-it">
			<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
			<i class="fa fa-check"></i> Berhasil Melakukan Pembayaran
		</div>
		<?php elseif ($this->session->flashdata('alert') == 'konfirmasi_sukses') : ?>
		<div class="alert-success animated fadeInDownBig hide-it">
			<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
			<i class="fa fa-check"></i> Berhasil Melakukan Konfirmasi
		</div>
		<?php elseif ($this->session->flashdata('alert') == 'pesan_sukses') : ?>
		<div class="alert-success animated fadeInDownBig hide-it">
			<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
			<i class="fa fa-check"></i> Berhasil Melakukan Pemesanan
		</div>
		<?php elseif ($this->session->flashdata('alert') == 'pesan_hapus') : ?>
		<div class="alert-success animated fadeInDownBig hide-it">
			<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
			<i class="fa fa-check"></i> Berhasil Menghapus Pesanan
		</div>
		<?php elseif ($this->session->flashdata('alert') == 'komentar_sukses') : ?>
		<div class="alert-success animated fadeInDownBig hide-it">
			<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
			<i class="fa fa-check"></i> Berhasil Menambahkan Komentar
		</div>
		<?php endif;?>
	</div>



