  <!-- footer-section start -->
  <footer class="footer-section">
    <div class="footer-bottom">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-md-6">
            <p class="copy-right-text text-md-left text-center mb-md-0 mb-3">Copyright © 2021.All Rights Reserved By</p>
          </div>
         
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- footer-section end -->

  <!-- scroll-to-top start -->
  <div class="scroll-to-top">
    <span class="scroll-icon">
      <i class="fa fa-rocket"></i>
    </span>
  </div>
  <!-- scroll-to-top end --> 

  <script src="<?= base_url() ?>assets/frontend/js/jquery-3.3.1.min.js"></script>
  <script src="<?= base_url() ?>assets/frontend/js/bootstrap.min.js"></script>
  <script src="<?= base_url() ?>assets/frontend/js/jquery.nice-select.js"></script>
  <script src="<?= base_url() ?>assets/frontend/js/lightcase.js"></script>
  <script src="<?= base_url() ?>assets/frontend/js/owl.carousel.min.js"></script>
  <script src="<?= base_url() ?>assets/frontend/js/jquery-ui.min.js"></script>
  <script src="<?= base_url() ?>assets/frontend/js/wow.min.js"></script>
  <script src="<?= base_url() ?>assets/frontend/js/jquery.waypoints.min.js"></script>
  <script src="<?= base_url() ?>assets/frontend/js/jquery.countup.min.js"></script>
  <script src="<?= base_url() ?>assets/frontend/js/jquery.paroller.min.js"></script>
  <script src="<?= base_url() ?>assets/frontend/js/main.js"></script>
  
<script src="<?=base_url()?>assets/backend/node_modules/dropify/dist/js/dropify.min.js"></script>
<script src="<?=base_url()?>assets/backend/js/dropify.js"></script>
<script src="<?=base_url()?>assets/frontend/js/sipesan.js"></script>
</body>

</html>




